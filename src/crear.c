#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/funciones.h"
//FUNCION QUE CREA UN CAJON Y ASIGNA MEMORIA
void crear_cajon(cajon_t *cajon){
    memset(cajon, 0, sizeof(cajon_t));
    cajon->cant_bloques=0;
    cajon->memoria = malloc(1000);
    if(cajon->memoria == NULL){
        printf("Problemas al asignar memoria al cajon\n");
        exit(-1);
    }
    memset(cajon->memoria, 0, 1000);
    cajon->espacio = 1000;
}
//FUNCION PARA CREAR BLOQUE
void crear_bloques(long int *usr, cajon_t *cajon){
    while(1){
        memset(usr, 0, sizeof(long int));
        printf("Ingrese una cantidad de memoria: ");
        scanf("%ld", usr);        
        if(*usr <= sizeof(bloque_t)){
            printf("Ingrese una cantidad de memoria valida\n");
            printf("La cantidad de memoria a usar debe ser mayor a %ld bytes\n", sizeof(bloque_t));
            detalles(cajon);
            break;
        }
        if(*usr == 0){
            printf("Se dejo de crear bloques\n");
            cajon->bloques--;
            detalles(cajon);
            break;
        }
        long int resta = cajon->espacio-*usr;
        if(resta < 0){
            printf("No hay mas espacio en el cajon\n");
            cajon->bloques--;
            detalles(cajon);
            break;
        }
        else{
            printf("Cantidad correcta\n");
            bloque_t *bloque = (bloque_t *)(cajon->memoria + (1000-cajon->espacio));
            bloque->pos = cajon->cant_bloques+1;
            bloque->tamanio =*usr;
            bloque->estado = "ASIGNADO";
            cajon->bloques = bloque;
            cajon->cant_bloques += 1;
            cajon->espacio = resta;
            printf("espacio en el cajon: %ld\n", cajon->espacio);
            detalles(cajon);
        }
    }                       
}
