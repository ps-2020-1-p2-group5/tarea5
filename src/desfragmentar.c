#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/funciones.h"

void desfragmentar(cajon_t *cajon){
    if(cajon->cant_bloques == 0){
        printf("No hay bloques para desfragmentar\n");
    }
    else{
        cajon->bloques = (bloque_t *)cajon->memoria;       
        long int val = cajon->bloques->tamanio;
        long int resta = 0;
        for(long int i=0; i< cajon->cant_bloques; i++){
            bloque_t *bloque1 = (bloque_t *)cajon->memoria;
            if(bloque1->tamanio == sizeof(bloque_t)){
                cajon->memoria += bloque1->tamanio;
                bloque_t *b = (bloque_t *)cajon->memoria;
                val = b->tamanio;
            }else{
                if(i < cajon->cant_bloques-1){
                    bloque_t *bloque2 = (bloque_t *)(cajon->memoria + val);
                    val+= bloque2->tamanio;                    
                    resta = bloque1->tamanio - sizeof(bloque_t);
                    char *var = cajon->memoria + sizeof(bloque_t);
                    printf("direccion_nueva: %p\t direccion_anterior: %p\n", var, bloque2);
                    memset(var, 0, resta);
                    bloque1->tamanio = sizeof(bloque_t);
                    val -= bloque1->tamanio;
                    memmove(var, bloque2, bloque2->tamanio);
                    cajon->espacio += resta;
                    cajon->memoria += bloque1->tamanio;
                }
                if((i+1) == cajon->cant_bloques){
                    resta = bloque1->tamanio-sizeof(bloque_t);
                    char *var = cajon->memoria + sizeof(bloque_t);
                    printf("direccion: %p se liberan: %ld bytes\n", var, cajon->cant_bloques*resta);
                    memset(var, 0, resta);
                    cajon->espacio += resta;
                    bloque1->tamanio = sizeof(bloque_t);
                }
            }          
        }
        cajon->memoria = (char *)cajon->bloques;
        detalles(cajon);
    }    
}
