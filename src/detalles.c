#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/funciones.h"
//RECIBE CAJON Y MUESTRA CANTIDAD DE BLOQUES Y TAMAÑO
void detalles(cajon_t *cajon){
    printf("\nDetalles\n");
    cajon->bloques = (bloque_t *)cajon->memoria;
    printf("cajon{espacio: %ld, cant_bloques:%ld, memoria: %p}\n", cajon->espacio, cajon->cant_bloques, cajon);
    for(long int i=0; i < cajon->cant_bloques; i++){
        bloque_t *b = (bloque_t *)cajon->memoria;
        printf("bloque{pos: %ld, tamanio: %ld, estado: %s, memoria: %p}\n", b->pos, b->tamanio, b->estado, cajon->memoria);
        cajon->memoria += b->tamanio;
    }
    printf("\n");
    cajon->memoria = (char *)cajon->bloques;
}
