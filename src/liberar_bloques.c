#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/funciones.h"

void liberar_bloques(long int *usr, cajon_t *cajon){
    detalles(cajon);
    while(1){
        if(cajon->cant_bloques == 0){
            printf("No hay bloques que liberar\n");
            break;
        }else{            
            memset(usr, 0, sizeof(long));
            printf("Ingrese la posicion de uno de los bloques: ");
            scanf("%ld", usr);
            if(*usr == 0){
                printf("Se dejo de liberar bloques\n");
                break;
            }else{
                cajon->bloques = (bloque_t *)cajon->memoria;
                for(long i=0; i<cajon->cant_bloques; i++){
                    bloque_t *bloque = (bloque_t *)cajon->memoria;                    
                    if(*usr == bloque->pos){                        
                        long int val = bloque->tamanio;
                        long int tam = bloque->tamanio;
                        bloque->estado = "NO_ASIGNADO";
                        for(long int j= i; j < cajon->cant_bloques; j++){                            
                            if(j < cajon->cant_bloques - 1){
                                bloque_t *bloque2 = (bloque_t *)(cajon->memoria + val);
                                char *var = (char *)bloque2 - tam;
                                val += bloque2->tamanio;
                                bloque2->pos -= 1;
                                memset(var, 0, tam);
                                memmove(var, bloque2, bloque2->tamanio);
                                printf("direccion_anterior: %p\t direccion_nueva: %p\n\n", bloque2, var);
                            }
                        }
                        cajon->espacio += tam;
                        cajon->cant_bloques -= 1;
                        break;
                    }
                    cajon->memoria += bloque->tamanio;
                }
                cajon->memoria = (char *)cajon->bloques;
                detalles(cajon);
            }
        }
    }
}
