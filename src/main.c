#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/funciones.h"

//programa principal
int main(int argv, char **argc){
    cajon_t cajon;
    crear_cajon(&cajon);
    long int usr;
    printf("tamanio de cajon: %ld\n",sizeof(cajon_t));
    printf("tamanio de bloque: %ld\n",sizeof(bloque_t));
    while(1){
        printf("\n*******************MENU*******************\n");
        printf("1.- Crear bloques\n2.- Cambiar tamaño de bloques\n3.- Liberar bloques\n4.- Desfragmentar\n0.- Salir\n");
        memset(&usr, 0, sizeof(long));
        printf("Ingrese una opcion: ");
        scanf("%ld", &usr);
        if((usr==1)||(usr==2)||(usr==3)||(usr==4)||(usr==0)){
            menu(&usr, &cajon);
        }
        else{
            printf("Ingrese una opcion valida\n");            
        }
    }
}

void menu(long int *usr, cajon_t *cajon){
    switch(*usr){
        case 1: printf("1.- Crear bloques\n");
        crear_bloques(usr, cajon);
        break;

        case 2: printf("2.- Cambiar tamaño de bloques\n");
        cambiar_tamanio(usr, cajon);
        break;

        case 3: printf("3.- Liberar bloques\n");
        liberar_bloques(usr, cajon);
        break;

        case 4: printf("4.- Desfragmentar\n");
        desfragmentar(cajon);
        break;

        case 0: printf("0.- Salir\n");
        free(cajon->memoria);
        exit(-1);
        break;
    }
}
