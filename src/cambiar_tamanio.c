#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/funciones.h"

void cambiar_tamanio(long int *usr, cajon_t *cajon){
    long int usrn = 0;        
    while(1){
        cajon->memoria = (char *)cajon->bloques;
        detalles(cajon);
        if(cajon->cant_bloques == 0){
            printf("No hay bloques para cambiar tamanio\n");
            break;
        }else{
            memset(&usrn, 0, sizeof(long));
            printf("Ingrese la posicion de uno de los bloques ");
            scanf("%ld", &usrn);
            if(usrn == 0){
                printf("Se dejo de cambiar el tamanio de los bloques\n");
                break;
            }            
            cajon->bloques = (bloque_t *)cajon->memoria;
            for(long int i=0; i < cajon->cant_bloques; i++){
                bloque_t *bloque1 = (bloque_t *)cajon->memoria;
                if(usrn == bloque1->pos){               
                    memset(usr, 0, sizeof(long));
                    printf("Ingrese el nuevo tamanio: ");
                    scanf("%ld", usr);
                    long int tam = bloque1->tamanio;
                    long int val;
                    if(cajon->espacio < *usr-bloque1->tamanio){
                        printf("No hay espacio suficiente para agrandar los bloques\n");
                        cajon->memoria = (char *)cajon->bloques;
                        break;
                    }

                    if((tam-*usr >= sizeof(bloque_t)) && (*usr < tam)){
                        val = tam;
                        char *var = (char *)bloque1 + *usr;
                        for(long int j=i; j < cajon->cant_bloques; j++){
                            if(j < cajon->cant_bloques-1){
                                bloque_t *bloque2 = (bloque_t *)(cajon->memoria + val);                                
                                printf("direccion_anterior: %p\t direccion_nueva: %p\n", bloque2, var);
                                bloque1->tamanio = *usr;
                                val += bloque2->tamanio;
                                memset(var, 0, *usr);
                                memmove(var, bloque2, bloque2->tamanio);
                                var = (char *)bloque2 + *usr;
                                printf("direccion_siguiente: %p\n\n", cajon->memoria + val);
                            }if((j+1) == cajon->cant_bloques){
                                long int resta = tam - *usr;
                                bloque1->tamanio = *usr;
                                memset(var, 0, resta);
                            }
                        }
                        cajon->espacio += tam - *usr;                       
                        break;
                    }
                    
                    if((*usr-tam <= cajon->espacio) && (*usr > tam)){
                        val = 0;
                        long int veces = cajon->cant_bloques - usrn;
                        long int tamanios[veces];
                        long int resta = *usr-bloque1->tamanio;
                        for(long int j=0; j<veces; j++){
                            bloque_t *bloquen = (bloque_t *)(cajon->memoria +val);
                            tamanios[j] = bloquen->tamanio;
                            val += bloquen->tamanio;                            
                        }                        
                        for(long int j=i; j<cajon->cant_bloques; j++){
                            if(j < cajon->cant_bloques-1){
                                bloque_t *bloque2 = (bloque_t *)(cajon->memoria + val);
                                char *var = (char *)bloque2 + resta;
                                printf("direccion_anterior: %p\t direccion_nueva: %p\n", bloque2, var);                    
                                val -= tamanios[veces-1];
                                memmove(var, bloque2, bloque2->tamanio);
                                var -= resta;
                                printf("bloque_ant: %p\n", cajon->memoria + val);
                                veces--;
                                memset(var, 0, resta);
                            }
                            if(j+1 == cajon->cant_bloques){
                                bloque1->tamanio = *usr;
                                printf("direccion_bloque: %p\n\n", bloque1);
                                memmove(bloque1, bloque1, bloque1->tamanio);
                            }
                        }
                        cajon->espacio -= *usr-tam;
                        break;
                    }
                }
                cajon->memoria += bloque1->tamanio;
            }
            cajon->memoria = (char *)cajon->bloques;
        }
    }   
}
