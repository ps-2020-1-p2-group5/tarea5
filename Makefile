bin/tarea5: obj/main.o obj/cambiar_tamanio.o obj/desfragmentar.o obj/liberar_bloques.o obj/crear.o obj/detalles.o 
	gcc -Wall -fsanitize=address,undefined -g $^ -o $@

obj/main.o: src/main.c
	gcc -Wall -I .include/ -c $^ -o $@

obj/desfragmentar.o: src/desfragmentar.c
	gcc -Wall -I .include/ -c $^ -o $@

obj/liberar_bloques.o: src/liberar_bloques.c
	gcc -Wall -I .include/ -c $^ -o $@

obj/crear.o: src/crear.c
	gcc -Wall -I .include/ -c $^ -o $@

obj/detalles.o: src/detalles.c
	gcc -Wall -I .include/ -c $^ -o $@

obj/cambiar_tamanio.o: src/cambiar_tamanio.c
	gcc -Wall -I .include/ -c $^ -o $@

clean:
	rm bin/tarea5 obj/*.o
