
//estructuras a usar
typedef struct Bloque{
    long int pos;
    long int tamanio;
    char *estado;
}bloque_t;

typedef struct Cajon{
    long int espacio;
    long int cant_bloques;
    bloque_t *bloques;
    char *memoria;
}cajon_t;

//prototipos de funciones
void menu(long int *usr, cajon_t *cajon);
void crear_cajon(cajon_t *cajon);
void crear_bloques(long int *usr, cajon_t *cajon);
void cambiar_tamanio(long int *usr, cajon_t *cajon);
void liberar_bloques(long int *usr, cajon_t *cajon);
void desfragmentar(cajon_t *cajon);
void detalles(cajon_t *cajon);